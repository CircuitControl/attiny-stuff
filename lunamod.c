/* rewrite to normal C */
/*
*
* LunaMod for Arduino & Attiny45
* Remix by Rob Miles
* Tacoma, WA August 8th 2011
*
* I saw the original project in Make vol. 26
* by Brian McNamara
* Brian's was running on a PicAxe and I only have attiny45s so.......
*
* The freqout section where the real magic happens is from Paul Badger's synth code on the main Arduino site
*
* I kept this pretty straight forward but with an Arduino this could get a lot more complicated if you like
* Even on an attiny45 if you use you add a button to the led pin you coud sneak in more effects
*
*/
#include <stdlib.h>
#include <avr/io.h>
/* === globals ============================================================== */
int currentStep = 0;                                  //this is just to track which tone in memory we are currently playing

int steps[] = {500,500,100,100,100,100,100,100,                    // this is our tone storage areae
               100,100,100,100,100,100,100,100,                     //I used 64 tones or 8 tones per beat
               500,500,100,100,100,100,100,100,                     //you can change these manually and experiment if you like
               100,100,100,100,100,100,100,100,
               500,500,100,100,100,100,100,100,
               100,100,100,100,100,100,100,100,
               500,500,100,100,100,100,100,100,
               100,100,100,100,100,100,100,100};

int tempo = 0;                                      //tempo or speed between tones
int duration = 0;                                   //how long each of the 64 tones plays
int frequency = 0;                                  //current tone
int pitchval = 1;

#define FREQU_PIN (PB1)
#define TEMPO_PIN (PB1)
#define BUTTON_PIN (PB1)
#define LED_PIN (PB1)
#define SPEAKER_PIN (PB1)
/* === functions ============================================================ */
void lunamod_setup(void)
{

    /* set input pins */
    DDRB &= ~(1);
    /* set output pins */
    DDRB |= 1;


    #if 0
  pinMode (frequencyPot, INPUT);
  pinMode (tempoPot, INPUT);
  pinMode (buttonPin, INPUT);
  digitalWrite(buttonPin, HIGH);

  pinMode (ledPin, OUTPUT);
  pinMode (speakerPin, OUTPUT);
    #endif
}

void lunamod_loop(void)
{
}

int main(void)
{
    lunamod_setup();
    while(1)
    {
        lunamod_loop();
    }
}
#if 0
#define frequencyPot   0                              //frequency pot tied to pin 15 which is A1
#define tempoPot       1                              //tempo pot tied to pin 16 which is A2
#define buttonPin      9                              //programming button tied to pin 17 which is A3
#define ledPin         10                             //status led tied to pin 18 which is A4
#define speakerPin     11                             //speaker or output pin 19 whish is A5
                                                      //if you use a speaker it should be at least a 16 ohm speaker an should have a
                                                      //resistor, maybe 200ohm to 1K ohm, between the negative lead and ground.
                                                      //a potentiometer would be even better.


void setup()                                          //set up your pins....
{
  pinMode (frequencyPot, INPUT);
  pinMode (tempoPot, INPUT);
  pinMode (buttonPin, INPUT);
  digitalWrite(buttonPin, HIGH);

  pinMode (ledPin, OUTPUT);
  pinMode (speakerPin, OUTPUT);

}

void loop()
{


  for (int i=0; i<63; i++)                         //64 individual notes played
  {

  currentStep = i;                                 //save our current position in the loop for later

  if (i == 0 || i == 16 || i == 32 || i == 48){    //keep track of the beat on the led
  digitalWrite(ledPin, HIGH);}

  if (i == 7 || i == 23 || i == 39 || i == 55){    //keep track of the beat on the led
  digitalWrite(ledPin, LOW);}





  if (digitalRead(buttonPin) == LOW)               //is the program button being pressed
    {                                              //if so lets write a new tone the this location

    steps[currentStep] = (analogRead(frequencyPot));  //read the frequency pot and set the new tone
    freqout (steps[currentStep], duration);           //set the parameters for frequout below and play it
    freqout (steps[currentStep]+64, duration);       //play another tone a little bit different than the original to give
                                                      //it a little more depth. this can be changed to your liking
    freqout (steps[currentStep]+128, duration);       //play another tone a little bit different than the original to give
                                                      //it a little more depth. this can be changed to your liking

    }

    else {                                            //else play the tone

    freqout (steps[currentStep], duration);           //set the parameters for frequout below and play it
    freqout (steps[currentStep]+64, duration);       //play another tone a little bit different than the original to give
                                                      //it a little more depth. this can be changed to your liking
    freqout (steps[currentStep]+128, duration);       //play another tone a little bit different than the original to give
                                                      //it a little more depth. this can be changed to your liking


  }
    tempo = (analogRead(tempoPot)/4);                    //read the tempo pot
    duration = tempo/8;                                   //set the individual tone durations
    delay(tempo);                                         //wait a bit
}
}


void freqout(int freq, int t)
{
  int hperiod;
  long cycles, i;

  hperiod = (500000 / ((freq - 7) * pitchval));

  cycles = ((long)freq * (long)t) / 1000;

  for (i=0; i<= cycles; i++)
  {
    digitalWrite(speakerPin, HIGH);
    delayMicroseconds(hperiod);
    digitalWrite(speakerPin, LOW);
    delayMicroseconds(hperiod - 1);
  }
}
#endif