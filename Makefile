# $Id$

#
# Makefile for Attiny Projects
#
# Usage:
# 	> make [all]
#		to build the AttinyBassDrumTest.hex
#	> make clean
#		to remove all generated files.
#
DUDEOPTS = -P /dev/ttyUSB0 -c stk500v2 -b 115200
#DUDEOPTS = -P usp -c usbasp

all: AttinyBassDrumTest.hex lunamod.hex

%.hex: %.elf
	avr-objcopy -O ihex $< $@

%.elf: %.cpp
	avr-g++ -mmcu=attiny45 -O2 -Wall $< -o $@

%.elf: %.c
	avr-gcc -mmcu=attiny45 -O2 -Wall $< -o $@

clean:
	rm -rvf *.elf *.hex *.o

flash: fdrum

fdrum: AttinyBassDrumTest.hex
	avrdude $(DUDEOPTS) -p attiny45 -U lf:w:0xe2:m -U fl:w:$<:i

fluna: lunamod.hex
	avrdude $(DUDEOPTS) -p attiny45 -U lf:w:0xe2:m -U fl:w:$<:i
